package tree_map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * This class implements interface {@link Map}.
 * This class using {@link Logger} to log data.
 * Implements such methods as "put", "get", "remove" etc.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class SimpleTreeMap<K extends Comparable, V> implements Map<K, V> {

    private Logger log = LogManager.getLogger(SimpleTreeMap.class);

    private int size = 0;
    /**
     * Is the main root Entry {@link SimpleTreeMap.Node}
     */
    private Node<K, V> root;

    /**
     * @return - value of {@link SimpleTreeMap.Node#value} found by key {@link SimpleTreeMap.Node#key}.
     */
    @Override
    public V get(Object key) {
        V val;
        try {
            val = getNode(key, root).value;
        } catch (NullPointerException e) {
            val = null;
        }
        return val;
    }

    /**
     * Private method helps to find Node by key.
     *
     * @param key - key of Node that will be found.
     * @param parent - Is a Node, can be root {@link SimpleTreeMap#root}.
     * @return - Node object {@link SimpleTreeMap.Node}.
     */
    private Node<K, V> getNode(Object key, Node<K, V> parent) {
        Node<K, V> node;
        int cmp = compareNodes((K) key, parent);
        if (cmp > 0) {
            if (parent.right == null) {
                node = null;
            } else {
                node = getNode(key, parent.right);
            }
        } else if (cmp < 0) {
            if (parent.left == null) {
                node = null;
            } else {
                node = getNode(key, parent.left);
            }
        } else {
            node = parent;
        }
        log.debug(node);
        return node;
    }

    /**
     * Method add new object with unique key.
     * @param key - new unique object.
     * @param value - new value with its key.
     * @return - value that added by this method.
     */
    @Override
    public V put(K key, V value) {
        if (root == null) {
            root = new Node<>(key, value);
            size++;
        } else {
            putNode(new Node<>(key, value), root);
        }
        return value;
    }

    /**
     * This method add Node by its binary sequence.
     * Also helps to with removing Node from Map {@link SimpleTreeMap#remove(Object)}.
     *
     * @param node - Node object that must be added to Map.
     * @param parent - From what Node object its must compared.
     */
    private void putNode(Node<K, V> node, Node<K, V> parent) {
        K key = node.key;
        V value = node.value;
        int cmp = compareNodes(key, parent);
        if (cmp > 0) {
            if (parent.right == null) {
                parent.right = node;
                size++;
            } else {
                putNode(node, parent.right);
            }
        } else if (cmp < 0) {
            if (parent.left == null) {
                parent.left = node;
                size++;
            } else {
                putNode(node, parent.left);
            }
        } else {
            parent.value = value;
        }
        log.trace(node);
    }

    /**
     * Compare keys between Node objects.
     *
     * @param key - key of Node object.
     * @param parentNode - with which Node object its will be compare.
     * @return - -1, 0 or 1.
     */
    private int compareNodes(K key, Node<K, V> parentNode) {
        return key.compareTo(parentNode.key);
    }

    /**
     * Remove Node object by key.
     *
     * @param key - key of Node object
     * @return - value of removed Node object.
     */
    @Override
    public V remove(Object key) {
        V val;
        if (isEmpty()) {
            val = null;
        } else if (size == 1) {
            val = root.value;
            root = null;
            size--;
        } else {
            val = removeNode(key);
            if (val != null) {
                size--;
            }
        }
        return val;
    }

    private V removeNode(Object key) {
        Node<K, V> parent = findParent(key, root);
        if (parent == null) {
            return null;
        }
        Node<K, V> toRemove = getNode(key, parent);
        return deleteAndChange(parent, toRemove);
    }

    private V deleteAndChange(Node<K, V> parent, Node<K, V> toRemove) {
        if (parent.right == toRemove) {
            change(parent, toRemove);
        } else {
            change(parent, toRemove);
        }
        return toRemove.value;
    }

    /**
     * Will form new sequence in Map by changing with its Node objects.
     * Also will remove Node object.
     *
     * @param parent - parent of Node object to remove.
     * @param change - the object will be changed by another which mean will removed.
     */
    private void change(Node<K, V> parent, Node<K, V> change) {
        if (change.right != null && change.left != null) {
            Node<K, V> left = change.left;
            parent.right = change.right;
            putNode(left, parent.right);
        } else if (change.right == null && change.left == null) {
            parent.right = null;
        } else if (change.right != null) {
            parent.right = change.right;
        } else {
            parent.right = change.left;
        }
    }

    /**
     * Will find parent of Node object by key of object.
     *
     * @param key - key of Node object, parent we want to find.
     * @param node - Node object from which starts searching.
     * @return - parent of Node object, key we put here.
     */
    private Node<K, V> findParent(Object key, Node<K, V> node) {
        Node<K, V> parent;
        int cmp = compareNodes((K) key, node);
        if (cmp > 0) {
            if (node.right == null) {
                return null;
            }
            if (node.right.key == key) {
                parent = node;
            } else {
                parent = findParent(key, node.right);
            }
        } else if (cmp < 0) {
            if (node.left == null) {
                return null;
            }
            if (node.left.key == key) {
                parent = node;
            } else {
                parent = findParent(key, node.left);
            }
        } else {
            parent = node;
        }
        return parent;
    }

    /**
     * @return - size of {@link SimpleTreeMap}
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * @return - boolean is {@link SimpleTreeMap} is empty.
     */
    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    /**
     * Inner class of {@link SimpleTreeMap}.
     * The object through we create a binary tree.
     *
     * @param <K> - object that will be unique.
     * @param <V> - value that will saved with key in Node object.
     */
    private static class Node<K, V> implements Map.Entry<K, V> {

        private Node<K, V> left;
        private Node<K, V> right;
        private K key;
        private V value;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        @Override
        public String toString() {
            K leftKey;
            V leftValue;
            K rightKey;
            V rightValue;
            if (left == null) {
                leftKey = null;
                leftValue = null;
            } else {
                leftKey = left.key;
                leftValue = left.value;
            }
            if (right == null) {
                rightKey = null;
                rightValue = null;
            } else {
                rightKey = right.key;
                rightValue = right.value;
            }
            return "Node{" +
                    "key=" + key +
                    ", value=" + value +
                    ", left[K=" + leftKey + ",V=" + leftValue +
                    "], right[K=" + rightKey + ",V=" + rightValue +
                    "]}";
        }
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

}
