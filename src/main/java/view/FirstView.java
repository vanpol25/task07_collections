package view;

import controller.FirstMenu;

import java.util.Scanner;

/**
 * Is a class view that has method {@link FirstView#start}.
 * This show the day of week by Scanner input.
 * Class pattern is singleton.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class FirstView {

    private static FirstView firstView;
    private FirstMenu firstMenu = FirstMenu.getFirstMenu();
    private Scanner sc = new Scanner(System.in);

    private FirstView() {

    }

    public static FirstView getFirstView() {
        if (firstView == null) {
            firstView = new FirstView();
        }
        return firstView;
    }

    public void start() {
        int i = 3;
        do {
            i--;
            System.out.println("Choose your day: 1-7");
            int n = sc.nextInt();
            System.out.println("Its " + firstMenu.getDay(n) + "!");
        } while (i > 0);
        System.out.println("bye");
    }

}
