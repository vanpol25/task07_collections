package view;

import controller.SecondMenu;

import java.util.Scanner;

/**
 * Is a class view that has method {@link SecondView#start}.
 * This show the day of week by Scanner input.
 * Class pattern is singleton.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class SecondView {
    private SecondMenu secondMenu = SecondMenu.getSecondMenu();
    private static SecondView secondView;
    private Scanner sc = new Scanner(System.in);

    private SecondView() {

    }

    public static SecondView getSecondView() {
        if (secondView == null) {
            secondView = new SecondView();
        }
        return secondView;
    }

    public void start() {
        int i = 3;
        do {
            i--;
            System.out.println("Choose your day: 1-7");
            int n = sc.nextInt();
            System.out.println("Its " + secondMenu.getDay(n).name() + "!");
        } while (i > 0);
        System.out.println("bye");
    }

}
