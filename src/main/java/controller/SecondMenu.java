package controller;

import model.DayOfWeek;

import java.util.HashMap;
import java.util.Map;

/**
 * Is a class controller that has method {@link SecondMenu#getDay(int)}.
 * This method return days of week by its number using HashMap.
 * Class pattern is singleton.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class SecondMenu {

    private static SecondMenu secondMenu;
    private Map<Integer, DayOfWeek> dayOfWeekMap = new HashMap<>();

    private SecondMenu() {

    }

    public static SecondMenu getSecondMenu() {
        if (secondMenu == null) {
            secondMenu = new SecondMenu();
        }
        return secondMenu;
    }
    {
        dayOfWeekMap.put(1, DayOfWeek.MONDAY);
        dayOfWeekMap.put(2, DayOfWeek.TUESDAY);
        dayOfWeekMap.put(3, DayOfWeek.WEDNESDAY);
        dayOfWeekMap.put(4, DayOfWeek.THURSDAY);
        dayOfWeekMap.put(5, DayOfWeek.FRIDAY);
        dayOfWeekMap.put(6, DayOfWeek.SATURDAY);
        dayOfWeekMap.put(7, DayOfWeek.SUNDAY);

    }

    public DayOfWeek getDay(int number) {
        return dayOfWeekMap.get(number);
    }
}
