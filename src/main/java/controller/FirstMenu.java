package controller;

import model.DayOfWeek;

/**
 * Is a class controller that has method {@link FirstMenu#getDay(int)}.
 * This method return days of week by its number.
 * Class pattern is singleton.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class FirstMenu {

    private static FirstMenu firstMenu;

    private FirstMenu() {

    }

    public static FirstMenu getFirstMenu() {
        if (firstMenu == null) {
            firstMenu = new FirstMenu();
        }
        return firstMenu;
    }

    public String getDay(int number) {
        String day;
        switch (number) {
            case 1: {
                day = DayOfWeek.MONDAY.name();
                break;
            }
            case 2: {
                day = DayOfWeek.TUESDAY.name();
                break;
            }
            case 3: {
                day = DayOfWeek.WEDNESDAY.name();
                break;
            }
            case 4: {
                day = DayOfWeek.THURSDAY.name();
                break;
            }
            case 5: {
                day = DayOfWeek.FRIDAY.name();
                break;
            }
            case 6: {
                day = DayOfWeek.SATURDAY.name();
                break;
            }
            case 7: {
                day = DayOfWeek.SUNDAY.name();
                break;
            }
            default: {
                day = "No such day!";
                break;
            }
        }
        return day;
    }

}
