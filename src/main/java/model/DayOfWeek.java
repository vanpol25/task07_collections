package model;

/**
 * Is a enum class that has all days of week.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public enum DayOfWeek {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY

}
