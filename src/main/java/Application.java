import tree_map.SimpleTreeMap;
import view.FirstView;
import view.SecondView;

public class Application {
    public static void main(String[] args) {
        SimpleTreeMap<Integer, Integer> stm = new SimpleTreeMap<>();
        stm.put(90, 90);
        stm.put(50, 50);
        stm.put(120, 120);
        stm.put(25, 25);
        stm.put(60, 60);
        stm.put(100, 100);
        stm.put(150, 150);
        stm.put(10, 10);
        stm.put(30, 30);
        stm.put(55, 55);
        stm.put(70, 70);
        stm.put(95, 95);
        stm.put(105, 105);
        stm.put(130, 130);
        stm.put(160, 160);

        stm.remove(120);
        stm.get(90);
        stm.get(120);
        stm.get(130);

        FirstView firstView = FirstView.getFirstView();
        firstView.start();
        SecondView secondView = SecondView.getSecondView();
        secondView.start();
    }
}
